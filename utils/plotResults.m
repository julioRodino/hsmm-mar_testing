% plotResults
% Script to plot all the basics HsMM modeling results outputs.
%
% ALAND ASTUDILLO FEB 2021
% addpath(genpath('hsmm10Feb2020'));
% addpath(genpath('utils'));

kcondition = 1;
ksubject = 1;
kblock = 1;
%data_struct.cond(kcondition).subj(ksubject).block(kblock).data = GEEG.data_prepared;

out_hsmm = training_output.out_hsmm;
sim_array = training_output.sim_array;
%hsmm_array = training_output.hsmm_array;
model_struct = training_output.model_struct;
%model_struct_array = training_output.model_struct_array;
%model_options = training_output.model_options;
hsmm_instance = training_output.hsmm_instance;
%training_time = training_output.training_time;
%lag = 8;
%% FIGURES: SHOW RESULTS SEQUENCE
states_sequence = out_hsmm.stateseq;
n_points = size(out_hsmm.stateseq, 2)+lag;
tvector = (0:n_points - (lag+1))/GEEG.fs_new; % time vector for plot
n_states = hsmm_instance.nstates;

%xlimite = [1, n_points/GEEG.fs_new];
xlimite = [1, 10];
ylimite = [-0.5, 5.5];
ylimite2 = [-0.5, n_states + 0.5];
amp = 0.15;

% figure;
% subplot(3,1,1); 
% plotMultiChan(data_struct.cond(1).subj(1).block(1).data, GEEG.fs_new, amp, xlimite, ylimite, 'input data', 'b');
% xlabel('Time s', 'FontSize', 12); ylabel('Components', 'FontSize', 12); 
% title('Input Data to the model', 'FontSize', 12);
% set(gca, 'FontSize', 12);

%subplot(2,1,2);
figure('position',[50,50,950,350])
plot(tvector, states_sequence); 
xlim(xlimite); ylim(ylimite2);
xlabel('Time s', 'FontSize', 12); ylabel('States', 'FontSize', 12);
set(gca, 'FontSize', 12);

% subplot(3,1,3); 
% plot(tvector, GEEG.data_prepared(1:end-lag, 1)); 
% xlim(xlimite);
% xlabel('Time s', 'FontSize', 12); ylabel('Amplitude', 'FontSize', 12); 
% set(gca, 'FontSize', 12);
% set(gcf, 'color', 'w');

% % %% FIGURE: PLOT FREE ENERGY
% % n_states = hsmm_instance.nstates;
% % 
% % figure;
% % y_out_FE = plotFreeEnergy(sim_array, 'data example', n_states, 'k', 0);
% % subplot(2,2,1); set(gca, 'FontSize', 12); xlim([0, size(y_out_FE, 2)+1]);
% % title('Last FE Values', 'FontSize', 12);
% % % subplot(2,2,2); title('FE Distribution', 'FontSize', 12); 
% % %set(gca, 'FontSize', 12);
% % subplot(2,2,3); set(gca, 'FontSize', 12); 
% % title('FE Evolution', 'FontSize', 12);
% % subplot(2,2,4); set(gca, 'FontSize', 12);
% % title('FE Evolution', 'FontSize', 12);

%% FIGURE: GET STATE MAPS USING PCA COEFF: CONDITION 1 SUBJ 1 BLOCK 1
% addpath(genpath('topo1'));

    n_states = hsmm_instance.nstates;
    nfil = floor((n_states + 2)/3);

    option_with_borders = 1;
    option_without_borders = 0;

    %hsmm_unit = model_struct.matrixmodel(1, 1, 1); 
    pca_coeff = GEEG.transform_options2.A;

    statemap = {};
    mu = {};
    for kstate = 1:training_output.out_hsmm.nstates %options.K
        mu{kstate} = training_output.hsmm_instance.emis_model.posterior.mean_normal{1, kstate}.mean;
        statemap{kstate} = mu{kstate} * pca_coeff'; 
    end

    if option_with_borders
    figure;
    for kstate = 1:training_output.out_hsmm.nstates
        subplot(3,nfil,kstate); 
    %     topoplot(statemap{kstate}, GEEG.chanlocs, 'electrodes', 'off'); %,'maplimits',escala);
        topoplot(statemap{kstate}, GEEG.chanlocs, 'electrodes', 'off', 'style', 'map');
        title(['S', num2str(kstate)], 'FontSize', 14);  
        colorbar; caxis('auto');
    end
    colormap redblue; %colormap jet;
    set(gcf, 'color', 'w');
    end

    if option_without_borders
    vch = [1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,...
           1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,...
           1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1];

    figure;
    for kstate = 1:n_states
        subplot(3,nfil,kstate); 
        stmap = statemap{kstate}; %beta(k,:) ; %statemap{k};
    %     topoplot(stmap(find(vch==1)), GEEG.chanlocs3, 'electrodes', 'off'); %,'shrink',0.01); %,'maplimits',escala);
        topoplot(stmap(find(vch==1)), GEEG.chanlocs3, 'electrodes', 'off', 'style', 'map');
        title(['S', num2str(kstate)], 'FontSize', 14); colorbar; caxis('auto');
    end
    colormap redblue;
    set(gcf, 'color', 'w');
    end
%% FIGURE: PLOT DURATION DISTRIBUTIONS FROM DURATION MODEL
n_states = hsmm_instance.nstates;
%hsmm_unit = model_struct.matrixmodel(1, 1, 1);

dur_points = 400;
dp = training_output.hsmm_instance.dur_model.prob('VB', (1:dur_points)');
tim = ((0:dur_points - 1))/GEEG.fs_new;
xlimite = [0, 0.8];
lab_list = {};

figure;
hold on;
%kstate = 4;
for kstate = 1:n_states
    plot(tim, dp(:, kstate), 'LineWidth', 2) ;%,colList{k}) % plot state k
    lab_list{kstate} = ['S', num2str(kstate)];
end
xlabel('Duration s', 'FontSize', 12); ylabel('Probability', 'FontSize', 12); 
title('Duration Model Distributions', 'FontSize', 14); 
set(gca, 'FontSize', 12);
xlim(xlimite); legend(lab_list);
set(gcf, 'color', 'w');

%% Grafica Modelo de duracion - Hernan's Version
figure;
hold on;
xlimite = [0, 0.8];
dur_points = 500;
t=(0:dur_points);
%t = t/GEEG.fs_new;
%dp = training_output.hsmm_instance.dur_model.prob('VB', (1:dur_points)');
for kstate = 1:n_states
    mn = training_output.hsmm_instance.dur_model.expectation.mean(kstate);
    mn = mn{1};
    prec = training_output.hsmm_instance.dur_model.expectation.prec(kstate);
    prec = prec{1};
    v = sqrt(1/prec);
    
    plot(t/GEEG.fs_new,lognpdf(t, mn, v),  'LineWidth', 2)
    %plot(tim, dp(:, kstate), 'LineWidth', 2) ;%,colList{k}) % plot state k
    lab_list{kstate} = ['S', num2str(kstate)];
end

xlabel('segundos', 'FontSize', 12); ylabel('Probability', 'FontSize', 12); 
title('Duration Model Distributions', 'FontSize', 14); 
set(gca, 'FontSize', 12);
xlim(xlimite); legend(lab_list);
set(gcf, 'color', 'w');

%% FIGURE: PLOT FO AND DURS MEASURES:
kblock = 1;
ksubject = 1;
kcondition = 1;

n_states = out_hsmm.nstates;
states_sequence = out_hsmm.stateseq;

[occupancy, duration_cat] = seqMeasures2(states_sequence, n_states);

% to count state's segments
n_chunks = zeros(1, n_states);
for kstate = 1:n_states
    n_chunks(1, kstate) = length(find(duration_cat(:, 2)==kstate));
end

% define colors
azul = [0.094118, 0.4549, 0.80392]; %blue
oran = [1, 0.49804, 0] ; % orange
yell = [1, 1, 0]; %'yellow';
viol = [0.61, 0.51, 0.74] ; %violet
red = [1, 0, 0]; %'red' ; %
gree = [0, 1, 0]; %'green' ; %
color_list = {azul,oran,yell,viol,red,gree,azul,oran,yell,viol,red,gree,...
             azul,oran,yell,viol,red,gree,azul,oran,yell,viol,red,gree};

factor = 1; 
nFil = n_states; 
nCol = 2;
ma = 1:nFil * nCol;
ma = reshape(ma, nCol, nFil)';
subpMat = ma(:, 2); %[2 , 4 , 6, 8];
xlimite = [0, 0.8]; % in seconds
nbins = 100; %50

figure;
for kstate = 1:nFil
    subplot(nFil,nCol,subpMat(kstate)); hold on;
    histogram(duration_cat(duration_cat(:, 2)==kstate, 1)/GEEG.fs_new, nbins,...
              'Normalization', 'probability')
    plot(tim, factor * dp(:, kstate), 'Color', color_list{kstate}, 'LineWidth', 2);
    ylabel(['S', int2str(kstate)], 'FontSize', 12);
    set(gca, 'FontSize', 12); xlim(xlimite); 
%     if kstate<nFil; set(gca,'XTick',[]); end
end
xlabel('Duration s', 'FontSize', 12);

subplot(3,2,1); hold on;
for kstate = 1:n_states
    bar(kstate, occupancy(kstate), 'FaceColor', color_list{kstate}); 
end
title('State sequence metrics', 'FontSize', 14);
ylabel('%FO', 'FontSize', 12); xlabel('State', 'FontSize', 12);
set(gca, 'FontSize', 12); xlim([0.5, n_states + 0.5]);

subplot(3,2,3); hold on;
for kstate = 1:n_states
    bar(kstate, n_chunks(kstate), 'FaceColor', color_list{kstate}); 
end
% title('#state segments Sub1','FontSize',14);
ylabel('N Segments', 'FontSize', 12); xlabel('State', 'FontSize', 12);
set(gca, 'FontSize', 12); xlim([0.5, n_states + 0.5]);

subplot(3,2,5); hold on;
boxplot(duration_cat(:, 1)/GEEG.fs_new, duration_cat(:, 2), 'notch', 'on'); 
plot(duration_cat(:, 2), duration_cat(:, 1)/GEEG.fs_new, 'k.')
% xlim([0.5, n_states + 0.5]); % ylim([0, 2.5]);
ylabel('Duration s', 'FontSize', 12); xlabel('State', 'FontSize', 12);
set(gca, 'FontSize', 12); 
set(gcf, 'color', 'w');

%% Figure: subject fo

if exist('T','var')
    figure;
    % define colors
    azul = [0.094118, 0.4549, 0.80392]; %blue
    oran = [1, 0.49804, 0] ; % orange
    yell = [1, 1, 0]; %'yellow';
    viol = [0.61, 0.51, 0.74] ; %violet
    red = [1, 0, 0]; %'red' ; %
    gree = [0, 1, 0]; %'green' ; %

    color_list = {azul,oran,yell,viol,red,gree,azul,oran,yell,viol,red,gree,...
             azul,oran,yell,viol,red,gree,azul,oran,yell,viol,red,gree};
    n_point_diff = size(states_sequence,2)-size(GEEG.data_prepared,1); % difference between number of points in data and stateseq
    if iscell(T)
        T = cell2mat(T);
    end
    csum = cumsum(T);
    point_mat = {};
    point_mat{1} = 1:T(1)+n_point_diff;
    for cc = 2:size(T,1)
        point_mat{cc} = point_mat{cc-1}(end)+1:csum(cc)+n_point_diff;
    end
    ksubject = size(T,1);
    n_states = out_hsmm.nstates;
    states_sequence = out_hsmm.stateseq;
    for i = 1:ksubject
        if i ~= 1
            [occupancy, duration_cat] = seqMeasures2(states_sequence(point_mat{i}), n_states);
        else
            [occupancy, duration_cat] = seqMeasures2(states_sequence(point_mat{i}), n_states);
        end
        subplot(ksubject,1,i); hold on;
        for kstate = 1:n_states
            bar(kstate, occupancy(kstate), 'FaceColor', color_list{kstate});
            ylim([0 60])
        end
        title(['Subject ' num2str(i)], 'FontSize', 10)
        if i == round(ksubject/2)
            ylabel('%FO', 'FontSize', 12);
        elseif i == ksubject
            xlabel('State', 'FontSize', 12);
        end
        xlim([0.5, n_states + 0.5]); 
    end
end
% %% FIGURE: GET MODEL TRANSITION PROBABILITY MATRICES FROM TRAIN
% hsmm_unit = model_struct.matrixmodel(1, 1, 1); 
% TPM = hsmm_unit.trans_model.posterior.categ_dirichlet.conc;
% n_states = hsmm_unit.nstates;
% 
% TPMn = zeros(n_states, n_states); 
% % normalization of the row of the transition prob matrices:
% for kstate = 1:n_states
%     TPMn(kstate, :) = TPM(kstate, :)/sum(TPM(kstate, :));
% end
% 
% escala = [0, max(TPMn(:))];  %[0 0.8];
% 
% doplot_tpm = 2;
% 
% if doplot_tpm==2
% figure;
% subplot(2,1,1); hold on;
% imagesc(TPM); title('Model TPM');
% for ks = 1:n_states
% patch([-0.5+ks, 0.5+ks, 0.5+ks, -0.5+ks],...
%       [-0.5+ks, -0.5+ks, 0.5+ks, 0.5+ks], 'black'); 
% end
% % set(gca, 'XTick', [1:n_states]); set(gca, 'YTick', [1:n_states]);
% axis ij; xlim([0.5, n_states + 0.5]); ylim([0.5, n_states + 0.5]);
% xlabel('State n+1', 'FontSize', 12);
% ylabel('State n', 'FontSize', 12);
% set(gca, 'FontSize', 12);
% colormap jet; colorbar; axis square; 
% 
% subplot(2,1,2); hold on;
% imagesc(TPMn, escala); 
% for ks = 1:n_states
%     patch([-0.5+ks, 0.5+ks, 0.5+ks, -0.5+ks],...
%           [-0.5+ks, -0.5+ks, 0.5+ks, 0.5+ks], 'black'); 
% end
% axis ij; xlim([0.5, n_states + 0.5]); ylim([0.5, n_states + 0.5]);
% title('Normalized Model TPM');
% % set(gca, 'XTick', [1:n_states]); set(gca, 'YTick', [1:n_states]); 
% xlabel('State n+1', 'FontSize', 12);
% ylabel('State n', 'FontSize', 12);
% set(gca, 'FontSize', 12);
% colormap jet; colorbar; axis square;
% set(gcf, 'color', 'w');
% elseif doplot_tpm==1
%     figure;
%     subplot(1,1,1); hold on;
% imagesc(TPMn, escala); 
% for ks = 1:n_states
%     patch([-0.5+ks, 0.5+ks, 0.5+ks, -0.5+ks],...
%           [-0.5+ks, -0.5+ks, 0.5+ks, 0.5+ks], 'black'); 
% end
% axis ij; xlim([0.5, n_states + 0.5]); ylim([0.5, n_states + 0.5]);
% title('Normalized Model TPM');
% % set(gca, 'XTick', [1:n_states]); set(gca, 'YTick', [1:n_states]); 
% xlabel('State n+1', 'FontSize', 12);
% ylabel('State n', 'FontSize', 12);
% set(gca, 'FontSize', 12);
% colormap jet; colorbar; axis square;
% set(gcf, 'color', 'w');
% end
%     
% %% FIGURE: GET MANUAL (EMPIRICAL) TRANSITION MATRICES FOR EACH CONDITION
% kcondition = 1;
% ksubject = 1;
% kblock = 1;
% states_sequence = out_hsmm.stateseq.cond(1, kcondition).subj(ksubject).block(1, kblock).stateseq;
% n_states = out_hsmm.nstates.cond(1, kcondition).subj(1, ksubject).block(1, kblock).nstates;
% % get transition matrices from state sequence 
% ETPM = getTranMat(states_sequence, n_states);
% % set diagonals to zero, do not show auto transitions
% tranMatc12 = ETPM;
% for kstate = 1:n_states; 
%     tranMatc12(kstate, kstate) = 0; 
% end
% % normalize each row
% tranMatc12n = zeros(n_states, n_states);
% for kstate = 1:n_states
%     tranMatc12n(kstate, :) = tranMatc12(kstate, :)/sum(tranMatc12(kstate, :));
% end
% 
% escala = [0, max(tranMatc12n(:))] ; 
% 
% figure;
% subplot(2,1,1); imagesc(ETPM); 
% % set(gca, 'XTick', 1:n_states); set(gca, 'YTick', 1:n_states); 
% xlabel('State n+1', 'FontSize', 12);
% ylabel('State n', 'FontSize', 12);
% set(gca, 'FontSize', 12);
% axis square; colorbar; title({'Empirical TPM'});
% 
% subplot(2,1,2); 
% imagesc(tranMatc12n, escala); hold on;
% for ks = 1:n_states
% patch([-0.5+ks, 0.5+ks, 0.5+ks, -0.5+ks],...
%       [-0.5+ks, -0.5+ks, 0.5+ks, 0.5+ks], 'black'); 
% end
% % set(gca, 'XTick', 1:n_states); set(gca, 'YTick', 1:n_states); 
% colormap jet; colorbar; axis square;
% title({'Empirical TPM (NO Diag)'});
% xlabel('State n+1', 'FontSize', 12);
% ylabel('State n', 'FontSize', 12);
% set(gca, 'FontSize', 12);
% set(gcf, 'color', 'w');
