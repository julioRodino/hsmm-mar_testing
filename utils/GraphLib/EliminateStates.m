function [NewGraph,NewX]=EliminateStates(OldGraph,x)
%%%%%%%%%%
% [NewGraph,NewX] =EliminateStates(OldGraph,X)
% 
% This function generates a new graph (output: NewGraph) from an old graph
% (input: OldGraph) where the output graph is the subgraph of the input graph
% obtained by removing the states (and relevant edges) for which the entry
% in the input vector x is 0. It also returns the vector NewX which is the
% vector x with relevant entries removed.
%
% A graph is a structure with the following compulsory fields
%   A - a V by V adjacency matrix where V is the number of states
%   (vertices) in the graph. An adjacency matrix row i column j entry
%   contains the number of edges directed from state i to state j
%
%   EdgeLabels - a V by V cell. An entry in the cell is empty if the
%   corresponding entry in A is 0. Otherwise it is a character array in
%   which each row is the label of the corresponding edge. The labels are
%   arranged in rows. Note that this means that all the edges from state i
%   to state j must have names of the same length (or padded by spaces so
%   that they will be so).
%
%   StateLabels - a V by 1 cell. Each entry contains the string which is
%   the state name.
%
% This function supports also other fields in the graph structure. They
% should all be either length V vectors or VxV matrices.
%%%%%%%%%%%%%%%%%%%%%%%
% By Roee Lahav 2009  %
% roeela@bgu.ac.il    %
%%%%%%%%%%%%%%%%%%%%%%%

NonZeroIndex=find(x~=0);

FieldNames=fieldnames(OldGraph);

NFields=length(FieldNames);

for k=1:NFields
    FieldDimensions=size(OldGraph.(FieldNames{k}));
    if(any(FieldDimensions==1)) % if field is a vector
        Temp=OldGraph.(FieldNames{k})(NonZeroIndex);
    else % if field is a matrix
        Temp=OldGraph.(FieldNames{k})(NonZeroIndex,:);
        Temp=Temp(:,NonZeroIndex);
    end;
    NewGraph.(FieldNames{k})=Temp;
end;
NewX=x(NonZeroIndex);
