function [yout, FEMaxPos] = plotFreeEnergy(sim_array, setname, nStates, colorB, option)
% Function to plot Free energy distributions of HSMM models
% AA JAN 2020
nRepetitions = size(sim_array, 2);
nStatesInspected = size(sim_array, 1);

modelList = [];
for krep = 1:nRepetitions % through K models, columns of aux
    if ~isempty(sim_array(1, krep).fe)
        modelList = [modelList, krep];
    end
end

fedet = {};
feMax = [];
for kinspected = 1:nStatesInspected 
    for krep = 1:nRepetitions
    fedetail = [];
    if ~isempty(sim_array(kinspected, krep).fedetail)
        for khist = 1:length(sim_array(kinspected, krep).fedetail)
            fedetail(khist) = sim_array(kinspected, krep).fedetail(khist).fe;
        end
        feMax(kinspected, krep) = fedetail(khist); 
        fedet{kinspected, krep} = fedetail;
    end
    end
end

% FEMaxPos is the index of the best model of the N trained models
[FEMaxVal, FEMaxPos] = max(feMax);
if length(FEMaxVal) > 1
    [FEMaxVal2, FEMaxPos2] = max(FEMaxVal(modelList));
else 
    FEMaxVal2 = FEMaxVal;
    FEMaxPos2 = 0;
end
col = FEMaxPos2 + 1;
fil = FEMaxPos(col);

% figure; 
subplot(2,2,3); hold on;
plot(fedetail); 
if length(FEMaxVal) > 1
    plot(fedet{fil, col}, colorB, 'LineWidth', 2);
else
    plot(fedet{fil}, colorB, 'LineWidth', 2);
end
xlabel('Iterations'); ylabel('FE Value'); 
title({setname, ' FE Evolution'});

% xlim([0, length(fedetail)]);

subplot(2,2,4); hold on;
for kinspected = 1:nStatesInspected
    for krep = 1:nRepetitions
        plot(fedet{kinspected, krep});
    end
end
if length(FEMaxVal) > 1
    plot(fedet{fil, col}, 'r', 'LineWidth', 2);
else
    plot(fedet{fil}, 'r', 'LineWidth', 2);
end
xlabel('Iterations'); ylabel('FE Value'); 
title('FE Evolution'); % title([setname, ' FE Evo']);
% xlim([0, length(fedetail)]);

subplot(2,2,1); hold on;
if length(FEMaxVal) > 1
    plot(feMax(:, modelList), 'bo');
    plot(FEMaxPos(modelList), FEMaxVal(modelList), 'ro', 'MarkerFaceColor', colorB);
else
    plot(feMax, 'bo');
    plot(FEMaxPos, FEMaxVal, 'ro', 'MarkerFaceColor', colorB);
end
title('Last FE'); xlabel('Repetition'); ylabel('FE Value');

if option==3
    subplot(2,2,2); hold on;
    if length(FEMaxVal) > 1
    %     boxplot(feMax(:, modelList)); % MUST BE CORRECTED FOR SEVERAL FE VALUES
    %     bplot(feMax(:, modelList)', modelList)
        plot(modelList, feMax(:, modelList)', 'b.', 'Color', colorB);
        ylabel('FE Value'); title('FE Dist'); xlabel('Number of States');
    %     set(gca, 'XTickLabels', modelList);
        xlim([modelList(1) - 0.5, modelList(end) + 0.5]);
    else
    %     boxplot(feMax); 
        plot(nStates, feMax, 'b.', 'Color', colorB);
        ylabel('FE Value'); title('FE Distribution'); xlabel('Number of States');
    %     set(gca, 'XTickLabels', num2str(nStates));
        xlim([nStates - 0.5, nStates + 0.5]);
    end    
else
    subplot(2,2,2); hold on;
    if length(FEMaxVal) > 1
        boxplot(feMax(:, modelList)); % MUST BE CORRECTED FOR SEVERAL FE VALUES
        plot(feMax(:, modelList)', 'b.', 'Color', colorB);
        ylabel('FE Value'); title('FE Dist'); xlabel('Number of States');
%         set(gca, 'XTickLabels', modelList);
    else
        boxplot(feMax); 
        plot(1, feMax, 'b.', 'Color', colorB);
        ylabel('FE Value'); title('FE Distribution'); xlabel('Number of States');
%         set(gca, 'XTickLabels', num2str(nStates));
    end
end
set(gcf, 'color', 'w');
yout = feMax; %1;
