function res = plotTransMat(tranMatc122, umbral, radio, plotauto, fsize)
% umbral = 5;
% radio = 0.8; %0.4; % define radious for circles
% plotauto = 0;
% fsize = 12; % define fontsize for states names

% define positions of centroids for each circle
% x  = [-1, 1, 1, -1]; y  = [-1, -1, 1, 1];
% x2 = [4, 6, 6, 4];   y2 = [-1, -1, 1, 1];
% x3 = [4, 6, 6, 4];   y3 = [4, 4, 6, 6];
% x4 = [-1, 1, 1, -1]; y4 = [4, 4, 6, 6];

azul = [0.094118, 0.4549, 0.80392]; %blue
oran = [1, 0.49804, 0] ; % orange
yell = 'yellow';
viol = [.61, .51, .74] ; %violet

%  ANCHO DEFAULT: 2;
ancho1 = tranMatc122(1, 2); % S1 TO S2 (1,2)
ancho2 = tranMatc122(2, 1); % S2 TO S1 (2,1)
ancho3 = tranMatc122(4, 1); % S4 TO S1 (4,1)
ancho4 = tranMatc122(1, 4); % S1 TO S4 (1,4)
ancho5 = tranMatc122(3, 2); % S3 TO S2 (3,2)
ancho6 = tranMatc122(2, 3); % S2 TO S3 (2,3)
ancho7 = tranMatc122(4, 3); % S4 TO S3 (4,3)
ancho8 = tranMatc122(3, 4); % S3 TO S4 (3,4)
ancho9 = tranMatc122(4, 2); % S4 TO S2 (4,2)
ancho10 = tranMatc122(2, 4); %S2 TO S4 (2,4)
ancho11 = tranMatc122(3, 1); %S3 TO S1 (3,1)
ancho12 = tranMatc122(1, 3); %S1 TO S3 (1,3)
% for the condition 2:
% anc1 = tranMatc222(1, 2); % S1 TO S2 (1,2)
% anc2 = tranMatc222(2, 1); % S2 TO S1 (2,1)
% anc3 = tranMatc222(4, 1); % S4 TO S1 (4,1)
% anc4 = tranMatc222(1, 4); % S1 TO S4 (1,4)
% anc5 = tranMatc222(3, 2); % S3 TO S2 (3,2)
% anc6 = tranMatc222(2, 3); % S2 TO S3 (2,3)
% anc7 = tranMatc222(4, 3); % S4 TO S3 (4,3)
% anc8 = tranMatc222(3, 4); % S3 TO S4 (3,4)
% anc9 = tranMatc222(4, 2); % S4 TO S2 (4,2)
% anc10 = tranMatc222(2, 4); %S2 TO S4 (2,4)
% anc11 = tranMatc222(3, 1); %S3 TO S1 (3,1)
% anc12 = tranMatc222(1, 3); %S1 TO S3 (1,3)

% figure; % h = figure(1); hold on;
% subplot(1,2,1);
hold on;
if plotauto 
    cir = circles(0, 0, trans2(1) + radio, 'Color', azul);
    cir2 = circles(5, 0, trans2(2) + radio, 'Color', oran);
    cir3 = circles(5, 5, trans2(3) + radio, 'Color', yell);
    cir4 = circles(0, 5, trans2(4) + radio, 'Color', viol);
else
    cir = circles(0, 0, radio, 'Color', azul);
    cir2 = circles(5, 0, radio, 'Color', oran);
    cir3 = circles(5, 5, radio, 'Color', yell);
    cir4 = circles(0, 5, radio, 'Color', viol);
end
% p  = patch(x, y, azul, 'EdgeColor', 'none'); % blue to other blue
% p2 = patch(x2, y2, oran, 'EdgeColor', 'none'); % red to orange
% p3 = patch(x3, y3, yell, 'EdgeColor', 'none');
% p4 = patch(x4, y4, viol, 'EdgeColor', 'none'); % violet
axis([-2 7, -2 7]);
if ancho1 > umbral
plot_arrow(1, -0.5, 4, -0.5, 'linewidth', ancho1, 'headwidth', 0.07,...
    'headheight', 0.15, 'color', azul, 'facecolor', azul, 'edgecolor', azul); %blue S1 TO S2
end
if ancho2 > umbral
plot_arrow(4, 0.5, 1, 0.5, 'linewidth', ancho2, 'headwidth', 0.07,...
    'headheight', 0.15, 'color', oran, 'facecolor', oran, 'edgecolor', oran); %orange S2 TO S1
end

if ancho3 > umbral
plot_arrow(-0.5, 4, -0.5, 1, 'linewidth', ancho3, 'headwidth', 0.07,...
    'headheight', 0.15, 'color', viol, 'facecolor', viol, 'edgecolor', viol); % violet S4 TO S1
end
if ancho4 > umbral
plot_arrow(0.5, 1, 0.5, 4, 'linewidth', ancho4, 'headwidth', 0.07,...
    'headheight', 0.15, 'color', azul, 'facecolor', azul, 'edgecolor', azul); %blue S1 TO S4
end

if ancho5 > umbral
plot_arrow(4.5, 4, 4.5, 1, 'linewidth', ancho5, 'headwidth', 0.07,...
    'headheight', 0.15, 'color', yell, 'facecolor', yell, 'edgecolor', yell); % yellow S3 TO S2
end
if ancho6 > umbral
plot_arrow(5.5, 1, 5.5, 4, 'linewidth', ancho6, 'headwidth', 0.07,...
    'headheight', 0.15, 'color', oran, 'facecolor', oran, 'edgecolor', oran); % oran S2 TO S3
end

if ancho7 > umbral
plot_arrow(1, 4.5, 4, 4.5, 'linewidth', ancho7, 'headwidth', 0.07,...
    'headheight', 0.15, 'color', viol, 'facecolor', viol, 'edgecolor', viol); %violet S4 TO S3
end
if ancho8 > umbral
plot_arrow(4, 5.5, 1, 5.5, 'linewidth', ancho8, 'headwidth', 0.07,...
    'headheight', 0.15, 'color', yell, 'facecolor', yell, 'edgecolor', yell); % yellow S3 TO S4
end

if ancho9 > umbral
plot_arrow(0.7, 4, 4, 0.7, 'linewidth', ancho9, 'headwidth', 0.07,...
    'headheight', 0.15, 'color', viol, 'facecolor', viol, 'edgecolor', viol); % diagonal S4 TO S2
end
if ancho10 > umbral
plot_arrow(4.3, 1, 1, 4.3, 'linewidth', ancho10, 'headwidth', 0.07,...
    'headheight', 0.15, 'color', oran, 'facecolor', oran, 'edgecolor', oran); % diagonal S2 TO S4
end

if ancho11 > umbral
plot_arrow(4, 4.3, 0.7, 1, 'linewidth', ancho11, 'headwidth', 0.07,...
    'headheight', 0.15, 'color', yell, 'facecolor', yell, 'edgecolor', yell); % diagonal % yellow S3 TO S1
end
if ancho12 > umbral
plot_arrow(1, 0.7, 4.3, 4, 'linewidth', ancho12, 'headwidth', 0.07,...
    'headheight', 0.15, 'color', azul, 'facecolor', azul, 'edgecolor', azul); % diagonal blue S1 TO S3
end

text(0 - 0.2, 0, 'S1', 'FontWeight', 'bold', 'FontSize', fsize); % Texto state 1: 0,0
text(5 - 0.2, 0, 'S2', 'FontWeight', 'bold', 'FontSize', fsize); %Texto state 2: 5,0
text(5 - 0.2, 5, 'S3', 'FontWeight', 'bold', 'FontSize', fsize); %Texto state 3: 5,5
text(0 - 0.2,5, 'S4', 'FontWeight', 'bold', 'FontSize', fsize); %Texto state 4: 0,5
% title('Transition Control', 'FontSize', 14);
axis square; axis off

res = 1;

% 
% subplot(1,2,2);
% hold on;
% if plotauto
%     cir = circles(0, 0, trans2(5) + radio, 'Color', azul);
%     cir2 = circles(5, 0, trans2(6) + radio, 'Color', oran);
%     cir3 = circles(5, 5, trans2(7) + radio, 'Color', yell);
%     cir4 = circles(0, 5, trans2(8) + radio, 'Color', viol);
% else
%     cir = circles(0, 0, radio, 'Color', azul);
%     cir2 = circles(5, 0, radio, 'Color', oran);
%     cir3 = circles(5, 5, radio, 'Color', yell);
%     cir4 = circles(0, 5, radio, 'Color', viol);
% end
% % p  = patch(x, y, azul, 'EdgeColor', 'none'); % blue to other blue
% % p2 = patch(x2, y2, oran, 'EdgeColor', 'none'); % red to orange
% % p3 = patch(x3, y3, yell, 'EdgeColor', 'none');
% % p4 = patch(x4, y4, viol, 'EdgeColor', 'none'); % violet
% axis([-2 7, -2 7]);
% if anc1 >u mbral
% plot_arrow(1, -0.5, 4,-0.5, 'linewidth', anc1, 'headwidth', 0.07,...
% 'headheight', 0.15, 'color', azul, 'facecolor', azul, 'edgecolor', azul); %blue S1 TO S2
% end
% if anc2>umbral
% plot_arrow( 4,0.5, 1,0.5,'linewidth',anc2,'headwidth',0.07,...
% 'headheight',0.15,'color',oran,'facecolor',oran,'edgecolor',oran); %orange S2 TO S1
% end
% 
% if anc3>umbral
% plot_arrow(-0.5, 4, -0.5, 1,'linewidth',anc3,'headwidth',0.07,...
% 'headheight',0.15,'color',viol,'facecolor',viol,'edgecolor',viol ); % violet S4 TO S1
% end
% if anc4>umbral
% plot_arrow(0.5, 1,0.5, 4 ,'linewidth',anc4,'headwidth',0.07,...
% 'headheight',0.15,'color',azul,'facecolor',azul,'edgecolor',azul); %blue S1 TO S4
% end
% 
% if anc5>umbral
% plot_arrow(4.5, 4, 4.5, 1,'linewidth',anc5,'headwidth',0.07,...
% 'headheight',0.15,'color',yell,'facecolor',yell,'edgecolor',yell ); % yellow S3 TO S2
% end
% if anc6>umbral
% plot_arrow(5.5, 1,5.5, 4 ,'linewidth',anc6,'headwidth',0.07,...
% 'headheight',0.15,'color',oran,'facecolor',oran,'edgecolor',oran ); % oran S2 TO S3
% end
% 
% if anc7>umbral
% plot_arrow( 1,4.5, 4,4.5,'linewidth',anc7,'headwidth',0.07,...
% 'headheight',0.15 ,'color',viol,'facecolor',viol,'edgecolor',viol); %violet S4 TO S3
% end
% if anc8>umbral
% plot_arrow( 4,5.5, 1,5.5,'linewidth',anc8,'headwidth',0.07,...
% 'headheight',0.15,'color',yell,'facecolor',yell,'edgecolor',yell  ); % yellow S3 TO S4
% end
% 
% if anc9>umbral
% plot_arrow( 0.7, 4, 4, 0.7,'linewidth',anc9,'headwidth',0.07,...
% 'headheight',0.15,'color',viol,'facecolor',viol,'edgecolor',viol ); % diagonal S4 TO S2
% end
% if anc10>umbral
% plot_arrow( 4.3, 1, 1, 4.3,'linewidth',anc10,'headwidth',0.07,...
% 'headheight',0.15 ,'color',oran,'facecolor',oran,'edgecolor',oran); % diagonal S2 TO S4
% end
% 
% if anc11>umbral
% plot_arrow( 4, 4.3, 0.7,1,'linewidth',anc11,'headwidth',0.07,...
% 'headheight',0.15,'color',yell,'facecolor',yell,'edgecolor',yell ); % diagonal % yellow S3 TO S1
% end
% if anc12>umbral
% plot_arrow( 1,0.7, 4.3,4,'linewidth',anc12,'headwidth',0.07,...
% 'headheight',0.15,'color',azul,'facecolor',azul,'edgecolor',azul ); % diagonal blue S1 TO S3
% end
% 
% text(0-0.2,0,'S1','FontWeight','bold','FontSize',fsize); % Texto state 1: 0,0
% text(5-0.2,0,'S2','FontWeight','bold','FontSize',fsize); %Texto state 2: 5,0
% text(5-0.2,5,'S3','FontWeight','bold','FontSize',fsize); %Texto state 3: 5,5
% text(0-0.2,5,'S4','FontWeight','bold','FontSize',fsize); %Texto state 4: 0,5
% title('Transition PD','FontSize',14);
% axis square; axis off
