% Due to the difference between the labels in the structural connectivity
% matrix and the output form the source reconstruction. The order in which
% the data is organized needs to be changed. Meaning that the index of
% the areas in the data matrix have to correspond to the labels of the
% structural connectivity matrix.
% Date: 03/07/2022
% Contact: julio.rodino@ug.uchile.cl
% usage: subj_relabel(data)
% data - must be a structure an eeg_rest field. eeg_rest = [areas x time]. 
%

%%
function eeg_rest = subj_relabel(data)
    % New data order
    eeg_rest(1,:) = data.eeg_rest(3,:);
    eeg_rest(2,:) = data.eeg_rest(5,:);
    eeg_rest(3,:) = data.eeg_rest(7,:);
    eeg_rest(4,:) = data.eeg_rest(9,:);
    eeg_rest(5,:) = data.eeg_rest(13,:);
    eeg_rest(6,:) = data.eeg_rest(15,:);
    eeg_rest(7,:) = data.eeg_rest(17,:);
    eeg_rest(8,:) = data.eeg_rest(21,:);
    eeg_rest(9,:) = data.eeg_rest(23,:);
    eeg_rest(10,:) = data.eeg_rest(25,:);
    eeg_rest(11,:) = data.eeg_rest(27,:);
    eeg_rest(12,:) = data.eeg_rest(29,:);
    eeg_rest(13,:) = data.eeg_rest(31,:);
    eeg_rest(14,:) = data.eeg_rest(35,:);
    eeg_rest(15,:) = data.eeg_rest(33,:);
    eeg_rest(16,:) = data.eeg_rest(37,:);
    eeg_rest(17,:) = data.eeg_rest(39,:);
    eeg_rest(18,:) = data.eeg_rest(41,:);
    eeg_rest(19,:) = data.eeg_rest(43,:);
    eeg_rest(20,:) = data.eeg_rest(45,:);
    eeg_rest(21,:) = data.eeg_rest(47,:);
    eeg_rest(22,:) = data.eeg_rest(49,:);
    eeg_rest(23,:) = data.eeg_rest(51,:);
    eeg_rest(24,:) = data.eeg_rest(53,:);
    eeg_rest(25,:) = data.eeg_rest(55,:);
    eeg_rest(26,:) = data.eeg_rest(57,:);
    eeg_rest(27,:) = data.eeg_rest(59,:);
    eeg_rest(28,:) = data.eeg_rest(61,:);
    eeg_rest(29,:) = data.eeg_rest(63,:);
    eeg_rest(30,:) = data.eeg_rest(67,:);
    eeg_rest(31,:) = data.eeg_rest(19,:);
    eeg_rest(32,:) = data.eeg_rest(4,:);
    eeg_rest(33,:) = data.eeg_rest(6,:);
    eeg_rest(34,:) = data.eeg_rest(8,:);
    eeg_rest(35,:) = data.eeg_rest(10,:);
    eeg_rest(36,:) = data.eeg_rest(14,:);
    eeg_rest(37,:) = data.eeg_rest(16,:);
    eeg_rest(38,:) = data.eeg_rest(18,:);
    eeg_rest(39,:) = data.eeg_rest(22,:);
    eeg_rest(40,:) = data.eeg_rest(24,:);
    eeg_rest(41,:) = data.eeg_rest(26,:);
    eeg_rest(42,:) = data.eeg_rest(28,:);
    eeg_rest(43,:) = data.eeg_rest(30,:);
    eeg_rest(44,:) = data.eeg_rest(32,:);
    eeg_rest(45,:) = data.eeg_rest(36,:);
    eeg_rest(46,:) = data.eeg_rest(34,:);
    eeg_rest(47,:) = data.eeg_rest(38,:);
    eeg_rest(48,:) = data.eeg_rest(40,:);
    eeg_rest(49,:) = data.eeg_rest(42,:);
    eeg_rest(50,:) = data.eeg_rest(44,:);
    eeg_rest(51,:) = data.eeg_rest(46,:);
    eeg_rest(52,:) = data.eeg_rest(48,:);
    eeg_rest(53,:) = data.eeg_rest(50,:);
    eeg_rest(54,:) = data.eeg_rest(52,:);
    eeg_rest(55,:) = data.eeg_rest(54,:);
    eeg_rest(56,:) = data.eeg_rest(56,:);
    eeg_rest(57,:) = data.eeg_rest(58,:);
    eeg_rest(58,:) = data.eeg_rest(60,:);
    eeg_rest(59,:) = data.eeg_rest(62,:);
    eeg_rest(60,:) = data.eeg_rest(64,:);
    eeg_rest(61,:) = data.eeg_rest(68,:);
    eeg_rest(62,:) = data.eeg_rest(20,:);
end

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    







