% pipeline_hsmm_example_MAR2
% Script to perform connectivity estimation using constraints using BSD
% toolbox.

% Steps:
% 1. Add toolbox to path
% 2. Load data
% 3. Define structural constraint
% 4. Initialization
% 5. Define the model
% 6. Train the model
% 7. Verify number of states
% 8. Get results
% 9. Plot state sequences
% 10. Verify error of the state sequence estimation
% 11. Show performance

% HERNAN HERNANDEZ - ALAND ASTUDILLO - JUN 2021

%% 1. Add folders to path
run 'eeglab2019_0/eeglab.m'
addpath(genpath('hsmm31Sep2020'));
addpath(genpath('utils'));
addpath(genpath('HMM-MAR-master 20180107'));
close all

%% 2. Load data
%load('Data/data_container.mat');
files = dir('data/source_noSegment/*.mat');
placebo = [2,6,9,10]; % Index of the placebo files in files variable
%data_container                = load(fullfile(files(1).folder,files(1).name));
[T, data_container.eeg_rest] = subjcat(files(placebo),1);
%data_container.eeg_rest = data_container.eeg_rest(:,1:round(7412171/10));
data_container.fs             = 500;
data_container.T              = T; 
data_container.subj           = 1;
data_container.block          = 1;
data_container.cond           = 1;
data_container.folder_name    = '10_01_D2_sensorData';
data_container.set_name       = '10_01_D2_sensorData';
data_container.set_mame_block = '10_01_D2_sensorData';
data_container.chanlocs       = [];
data_container.chanlocs3      = [];

data_container.data_prepro = double(data_container.eeg_rest)';
data_container = rmfield(data_container,'eeg_rest');
%% 3. Data transformation - transforming the data
% define basic transformation parameters:
tr_options.fs_new = 200; % define subsampling frequency value
tr_options.filter_band = [1, 30]; % Define frequency band for filtering
tr_options.do_hilbert = 0; % perform Hilbert transform to obtain envelope signal
tr_options.n_pca_components = 50; % define number of components for PCA
tr_options.do_log = 0;

transform_output = transformData(data_container, tr_options); 
T = transform_output.T3ds;
GEEG = transform_output.GEEG;
data = GEEG.data_prepared;

%% 4. Initialization
d_max = 250; % max duration 
n_states_max = 5; % max number of states 
n_data = size(data, 1); % number of data
if tr_options.n_pca_components
    n_channels = tr_options.n_pca_components;
else
    n_channels = size(data,2);
end
lag = 3;
%% 5. Define the model
n_states = 5;

emisionModel = emis_model.mar3(n_channels, n_states, lag);
durationModel = dur_model.normal_normal_gamma(1, n_states);

hsmm3 = hsmm(n_channels, n_states, emisionModel, [], [], durationModel);
hsmm3.priornoinf();

%[mask] = util.convertermar(structural_constraint, n_channels, lag);
%hsmm3.emis_model.prior.coef_mask = mask;

%% 6. Train the model
tic;
[out_hsmm, sim_array, hsmm_array, model_struct, model_struct_array] = hsmm3.train(data,...
    'maxitersim',12,...
    'tol', 1,...
    'maxcyc', 15,...
    'dmax', 250,...
    'nrep', 8,...
    'parallel', 1,...
    'initoption', 'random2',...
    'ncore', 16);
training_time = toc;
training_output.out_hsmm = out_hsmm;
training_output.sim_array = sim_array;
training_output.hsmm_array = hsmm_array;
training_output.model_struct = model_struct;
training_output.model_struct_array = model_struct_array;
%training_output.model_options = model_options;
training_output.hsmm_instance = hsmm3;
training_output.training_time = training_time;
%% Save training output
save('output/HsMM_output_MAR_PCA_source_placebo_fs_200_f1-30_D2.mat','training_output','GEEG', 'T')
%% 7. Verify number of states
%X = sprintf('Number of states %i', out_hsmm.nstates);
%disp(X);

%% 8. Get results
out_hsmm = training_output.out_hsmm;
stateseq = out_hsmm.stateseq;
plotResultsMAR;
%% Save figs
tempdir = 'figures/';
FolderName = tempdir;   % Your destination folder
FigList = findobj(allchild(0), 'flat', 'Type', 'figure');
for iFig = 1:length(FigList)
  FigHandle = FigList(iFig);
  FigName   = get(FigHandle, 'Name');
  savefig(FigHandle, fullfile(FolderName, [num2str(iFig) 'BIG.fig']));
end
close all
