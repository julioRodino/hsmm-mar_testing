# HsMM-MAR_testing
Repository to test BSD toolbox on real data. The problem until now has been that using the MAR model on real data results in short (1 point-long) state durations.

## DATA
Inside the data folder there is a segment and noSegment folder. The eeg_rest directory inside each of those is the result from lcmv beamforming with the fieldtrip toolbox. Segment and noSegment differ in the method for calculating the covariance matrix for the spatial filter. It is preferable to use the noSegmented data from previous discussions with the group.

The data is too large to upload to a github repository. A link to the source reconstruction data along with the electrode data will be provided.
The output for the models are also too big to be uploaded to github.

## HsMM-MAR pipelines
Previous scripts for analyzing the data with varied settings have been tested. Following the name of the standard scripts for HsMM-MAR analysis with real data that resulted from following the tutorial examples:

Most scripts are arranged in sections:
1. Add directories to matlab's path
2. Load data. In some scripts concatenation of multiple files can happen.
3. Data transformation and preprocessing. Filtering, n° of components and other steps like standarization are included in this.
4. Initialization options
5. Defining the model and passing the structural connectivity matrix.
6. Training the model
7. Saving Output
8. Ploting figures

Scripts:

### pipeline_hsmm_example_MAR.m 

This pipeline uses the concatenated data from multiple subjects. This includes the structural connectivity matrix. Preprocessing: Downsamples to 200 Hz. Filters from 1-30 Hz. Standardize data (z-score) per subject (this actually happens at the concatenation step in the subjcat function). If it takes too much RAM. The number of subjects analyzed (concatenated files) can be changes by specifying a fixed number in the concatenation function. Like so:

 `data_container.eeg_rest = subjcat(files(1:3),1))`

This will concatenate files 1 to 3 instead of every .mat file in the folder.

For test 2 of this pipeline we are trying with a filter from 1 - 70 Hz, 64000 data points from concatenated registers. Also fewer iterations to save time and memory. And n_states = 7. Let's go! ... Result of test with broader filter: Durations are too low again.

Trying with no filter...

### pipeline_hsmm_example_noConstraints.m

This pipeline uses concatenated source-data from multiple subjects. No structural connectivity matrix. Downsamples to 200 Hz. Filters from 1-30 Hz. Standardize data (z-score) per subject (this actually happens at the concatenation step in the subjcat function).

Updated to use T variable as explained in Oxford's wiki. Also using new version of the BSD toolbox.

New toolbox version is missing regressor_opt.m. After adding that script to +utils new error comes up:

Starting parallel pool (parpool) using the 'local' profile ...
Connected to the parallel pool (number of workers: 12).
Analyzing and transferring files to the workers ...done.
Error using inference.hsmmresidual_decodelog (line 86)
An UndefinedFunction error was thrown on the workers for 'ALPHATOTAL'.  This might be
because the file containing 'ALPHATOTAL' is not accessible on the workers. Use
addAttachedFiles(pool, files) to specify the required files to be attached.  For more
information, see the documentation for 'parallel.Pool/addAttachedFiles'.


Error in hsmm/decode (line 87)
            [decodevar] = inference.hsmmresidual_decodelog(self,X,varargin);

Error in inference.trainn (line 70)
            decodevar=self.decode(X,opt);

Error in hsmm/trainn (line 97)
            [out modelstruct]=inference.trainn(self,data,repi,varargin);

Error in inference.train (line 53)
            parfor j=1:opt.maxitersim

Error in hsmm/train (line 94)
            [out, sim_array, hsmm2, modelstruct,
            modelstructarray]=inference.train(self,data,varargin);

Error in pipeline_hsmm_example_MAR_noConstraints (line 93)
[out_hsmm, sim_array, hsmm_array, model_struct, model_struct_array] = hsmm3.train(data,...

Caused by:
    Unrecognized function or variable 'ALPHATOTAL'.


### pipeline_hsmm_mar_sensor.m

This pipeline uses data from sensors from one subject. No structural connectivity matrix is used. Crashes. Downsamples to 200 Hz. Filters from 1-30 Hz. Standardize data (z-score) per subject (this actually happens at the concatenation step in the subjcat function).

### pipeline_hsmm_example_MAR_PCA.m

This pipeline uses concatenated source-data from multiple subjects. No structural connectivity matrix. Downsamples to 200 Hz. Filters from 1-30 Hz. Standardize data (z-score) per subject (this actually happens at the concatenation step in the subjcat function). Uses 30 principal compents without hilbert envelope. MAR order of 3.

It takes about 135 GB of RAM.

Also updated. Uses T for transform.

This pipeline uses concatenated source-data from multiple subjects. No structural connectivity matrix. Downsamples to 60 Hz. Filters from 1-30 Hz. Standardize data (z-score) per subject (this actually happens at the concatenation step in the subjcat function). Uses 50 principal compents without hilbert envelope. MAR order of 3.

### pipeline_hsmm_example_MAR_PCA_small.m

This pipeline uses source-data from 1 subjects. No structural connectivity matrix. Downsamples to 200 Hz. Filters from 1-30 Hz. Standardize data (z-score) per subject (this actually happens at the concatenation step in the subjcat function). Uses 10 principal compents without hilbert envelope. MAR order of 3.

### pipeline_hsmm_mar_beamformer.m

Single subject source-data. It uses the structural connectivity matrix with 8 lags. Downsamples to 200 Hz. Filters from 1-30 Hz. No envelope.

For test 2 of this pipeline we are trying with a filter from 1 - 70 Hz 

### pipeline_hsmm_mar_beamformer_40.m

Single subject source-data. It uses the structural connectivity matrix wirh 40% threshold with 8 lags. Downsamples to 200 Hz. Filters from 1-30 Hz. No envelope

### pipeline_hsmm_mar_simulated
Using simulated data. 1-30 filter. Already at 200Hz, No hilbert. 8 lags, structural connectivity Matrix.

Result:
The model is not correctly estimating the duration probability of states. The first test was done using 5 states instead of the 7 states that the simulated data was based on.

Using 7 n_states didn't work. Problem is in another part of the pipeline. The third test is trying a broader spectrum for the bandpass filter. No filtering at all?

### pipeline_hsmm_sensor_MVN_placebo.m

Use HsMM-MVN with concatenated multiple subjects in the placebo condition. Input for the model is 30 components derived from the sensors. Filtering and resampling used. Filter from 8-12 Hz. Resampling from 500Hz to 250Hz. Using envelope and log.

### pipeline_hsmm_sensor_MVN_multisubject.m

Use HsMM-MVN with concatenated multiple subjects (4, 3 conditions each). Input for the model is 30 components derived from the sensors. Filtering and resampling used. Filter from 8-12 Hz. Resampling from 500Hz to 125Hz. Using envelope and log.

### pipeline_hsmm_source_MVN_placebo.m

Use HsMM-MVN with concatenated multiple subjects in the placebo condition. Input for the model is 30 components derived from the source. Filtering and resampling used. Filter from 8-12 Hz. Resampling from 500Hz to 250Hz. Using envelope and log.

### pipeline_hsmm_source_MVN_multisubject.m

Use HsMM-MVN with concatenated multiple subjects (4, 3 conditions each). Input for the model is 30 components derived from the source space. Filtering and resampling used. Filter from 8-12 Hz. Resampling from 500Hz to 125Hz. Using envelope and log.



## Tasks

1. Test a model with more than one subject with corrected normalization
2. Test different connectivity matrix with different thresholds.
3. Test a MAR model using PCA (single or multi subject). Using Sources
4. Test a MAR model using PCA (single or multi subject). Using Sensor

## Questions

Could we use PCA from reconstructed signals from LCMV? Does it make sense? Would that serve as input for MVN HsMM? (Plan C)
If we use PCA with MAR, does it make a difference on the order of the MAR? In Hernan's thesis an order of 5 appears to cause overfitting, and 3 is represented as a best solution -more generalized.

The model from simulated data had the same problem. This points to a problem with the code or, posibly, some preprocessing that is messing up the data (filter?). Though the PCA model with 1 subjects ended up having a duration probability function centered arround 0.1s. Is this usefull?

Apparently there are several ways to break a model. Using MVN the same trained model can give different results only changing the sampling frecuency. Sampling frequency changes a bit the results. But mostly duration model distributions stay the same.

## Ploting results

The ploting results function has been changed. Now is necessary to have loaded a variable called T with the durations of each subject in timepoints. T is expected to be a matrix of 1 x N° of subjects. This vector is returned by the transformData function. This has been implemented in MVN scripts not in MAR scripts yet.