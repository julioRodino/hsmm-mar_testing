% pipeline_hsmm_example
% Script to run basic pipeline for modeling data
% Steps:
% 1. Load toolboxes
% 2. Load data
% 3. Data transformation
% 4. Plot the transformed data
% 5. Setting the model training
% 6. Train the model
% 7. Plot the results
%
% ALAND ASTUDILLO JUN 2021

%% 1. Load toolboxes - add toolboxes to the path
% get % Oxford's toolbox for data transformation only
% addpath(genpath('HMM-MAR-master 20180107')); 
run 'eeglab2019_0/eeglab.m'
addpath(genpath('hsmm31Sep2020'));
addpath(genpath('utils'));

%% 2. Load data
%load('Data/data_container.mat');
data_container                = load('Data/10_01_D2_sourceData.mat');
data_container.fs             = 500;
data_container.T              = 500*120; % fs*sec
data_container.subj           = 1;
data_container.block          = 1;
data_container.cond           = 1;
data_container.folder_name    = '10_01_D2_sensorData';
data_container.set_name       = '10_01_D2_sensorData';
data_container.set_mame_block = '10_01_D2_sensorData';
data_container.chanlocs       = [];
data_container.chanlocs3      = [];

data_container.data_prepro = double(data_container.eeg_rest(:,1:data_container.T))';
data_container = rmfield(data_container,'eeg_rest');
%% 3. Data transformation - transforming the data
% define basic transformation parameters:
tr_options.fs_new = 64; % define subsampling frequency value
tr_options.filter_band = [7, 12]; % Define frequency band for filtering
tr_options.do_hilbert = 1; % perform Hilbert transform to obtain envelope signal
tr_options.n_pca_components = 20; % define number of components for PCA

transform_output = transformData(data_container, tr_options); 
GEEG = transform_output.GEEG;
% save('GEEGex2021.mat', 'GEEG');

%% 4. Plot the transformed data - Plot data and transformation steps
% LOAD GEEG
plotData;

%% 5. Setting the model training
% use a portion of data
n_points_data = 7680;
GEEG.data_prepared = GEEG.data_prepared(1:n_points_data, :);
GEEG.T_sum = n_points_data;
GEEG.T = n_points_data;

% configuring the model and define training settings
model_options.n_states = 5; % %6; 0; % put zero if you want to run in a range of states

model_options.min_states = 4; % Min number of states
model_options.max_states = 10; % Max number of states

model_options.duration_max = 80; % max number of points in time points for the duration

% data structure definition:  Cond | Subj | Block
model_options.se = [1,     1,       1]; % Emission model 
model_options.st = [1,     1,       1]; % Transition model 
model_options.si = [1,     1,       1]; % Initial State model 
model_options.sd = [1,     1,       1]; % Duration model 

model_options.type_cov = 'full'; % could be : 'diag'; %'full'; for MVN

% training settings:
model_options.max_iter_sim = 4; %N times to run the train (the final HSMM will be the one with the max FE)
model_options.init_option = 'random'; %'random' or 'kmeans'; % Initialization algorithm
model_options.n_repetitions = 5;	  % Number of times to initialize (for Kmeans initialization only)
model_options.tol = 10; % tolerancy
model_options.max_cyc = 150; % max cycle per running
model_options.parallel = 1; % put 1 if you want to use parallel computing option

%% 6. Training the model
% load('GEEGex2021.mat');
GEEG.data_prepared = gpuArray(GEEG.data_prepared);
tic
training_output = trainModel(GEEG, model_options);
training_time = toc /60; % in minutes
% save('model_ex2021.mat', 'training_output');

%% 7. Plot the results - PLOT HSMM RESULTS
% LOAD TRAINING_OUPUT
plotResults;
% figure;
% plot(linspace(1,20,64*20),training_output.out_hsmm.stateseq.cond.subj.block.stateseq(1:20*64));
% title('State Sequence')
% ylabel('State')
% xlabel('Time (s)');
%% Save figs
tempdir = 'fig_source';
FolderName = tempdir;   % Your destination folder
FigList = findobj(allchild(0), 'flat', 'Type', 'figure');
for iFig = 1:length(FigList)
  FigHandle = FigList(iFig);
  FigName   = get(FigHandle, 'Name');
  savefig(FigHandle, fullfile(FolderName, [num2str(iFig) '.fig']));
end
close all
