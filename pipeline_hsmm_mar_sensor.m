% pipeline_hsmm_example
% Script to run basic pipeline for modeling data
% Steps:
% 1. Load toolboxes
% 2. Load data
% 3. Data transformation
% 4. Plot the transformed data
% 5. Setting the model training
% 6. Train the model
% 7. Plot the results
%
% ALAND ASTUDILLO JUN 2021

%% 1. Load toolboxes - add toolboxes to the path
% get % Oxford's toolbox for data transformation only
% addpath(genpath('HMM-MAR-master 20180107')); 
run 'eeglab2019_0/eeglab.m'
addpath(genpath('hsmm31Sep2020'));
addpath(genpath('utils'));
close all
%% 2. Load data
%load('Data/data_container.mat');
%file_list = dir('epoched2/*.set');
load('data/sensor/10_01_D2_sensorData.mat');
eeg_rest                      = eeglab2fieldtrip(EEG,'preprocessing','none');
eeg_rest.trial                = eeg_rest.trial{1,1};
%eeg_rest.trial                = eeg_rest.trial(1:10,1:10000);
data_container.data_prepro    = double(eeg_rest.trial)';
data_container.fs             = 500;
data_container.T              = size(data_container.data_prepro,1); % fs*sec
data_container.subj           = 1;
data_container.block          = 1;
data_container.cond           = 1;
data_container.folder_name    = '10_01_D2_sensorData';
data_container.set_name       = '10_01_D2_sensorData';
data_container.set_mame_block = '10_01_D2_sensorData';
data_container.chanlocs       = [];
data_container.chanlocs3      = [];
%% 3. Data transformation - transforming the data
% define basic transformation parameters:
tr_options.fs_new = 200; % define subsampling frequency value
tr_options.filter_band = [1, 70]; % Define frequency band for filtering
tr_options.do_hilbert = 0; % perform Hilbert transform to obtain envelope signal
tr_options.n_pca_components = 0; % define number of components for PCA
tr_options.do_log = 0;

transform_output = transformData(data_container, tr_options); 
GEEG = transform_output.GEEG;
data = GEEG.data_prepared;
%% 4. Initialization
d_max = 250; % max duration 
n_states_max = 7; % max number of states 
n_iter = 30; % number of iterations
n_data = size(data, 1); % number of data
%lag = 3;

%[ini2] = util.genera_ini(lag, n_data, d_max, n_iter, n_states_max);

%% 5. Define the model
n_states = 5;
n_channels = size(data,2);
mar_order = 3;
emisionModel = emis_model.mar3(n_channels, n_states, mar_order);
%durationModel = dur_model.normal_normal_gamma(1, n_states);
durationModel = dur_model.lognormal_normal_gamma(1, n_states);

hsmm3 = hsmm(n_channels, n_states, emisionModel, [], [], durationModel);
hsmm3.priornoinf();
%load('structural_matrix_prob.mat')
%structural_matrix_prob = structural_matrix_prob(1:10,1:10,2:mar_order+1);
%structural_matrix_prob = ones([size(data,2),size(data,2),mar_order]);
%structural_matrix_prob = int32(structural_matrix_prob);
%[mask] = util.convertermar(structural_matrix_prob, n_channels, mar_order);
%hsmm3.emis_model.prior.coef_mask = mask;
%% Clear unnesesary variables
%% 6. Train the model
tic;
[out_hsmm, sim_array, hsmm_array, model_struct, model_struct_array] = hsmm3.train(data,...
    'maxitersim',60,...
    'tol', 1,...
    'maxcyc', 500,...
    'dmax', 250,...
    'nrep', 8,...
    'parallel', 1,...
    'initoption', 'random2',...
    'ncore', 16);
training_time = toc;
training_output.out_hsmm = out_hsmm;
training_output.sim_array = sim_array;
training_output.hsmm_array = hsmm_array;
training_output.model_struct = model_struct;
training_output.model_struct_array = model_struct_array;
%training_output.model_options = model_options;
training_output.hsmm_instance = hsmm3;
training_output.training_time = training_time;
%% Save training output
save('output/HsMM_output_MAR_sensor.mat','training_output','tr_options','GEEG')
% %% 7. Verify number of states
% X = sprintf('Number of states %i', out_hsmm.nstates);
% disp(X);
%% 8. Get results
out_hsmm = training_output.out_hsmm;
stateseq = out_hsmm.stateseq;
plotResults;
%% Save figs
tempdir = 'figures';
FolderName = tempdir;   % Your destination folder
FigList = findobj(allchild(0), 'flat', 'Type', 'figure');
for iFig = 1:length(FigList)
  FigHandle = FigList(iFig);
  FigName   = get(FigHandle, 'Name');
  savefig(FigHandle, fullfile(FolderName, [num2str(iFig) 'BIG.fig']));
end
close all
